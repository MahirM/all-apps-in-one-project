package hangman;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

/**
 * Created by Mahir on 27.08.2017..
 */
public class MyLabel extends Label {
    private String tag;
    private String color = ColorType.GREY.getColor();

    public MyLabel(){
        setStyle("-fx-background-color: " + color +"; -fx-background-radius: 10px");
        setTextFill(Color.BLACK);
        setPrefSize(25, 25);
        setMaxWidth(Double.MAX_VALUE);
        setAlignment(Pos.CENTER);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStyle("-fx-background-color: " + color);
    }


}
