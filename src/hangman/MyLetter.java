package hangman;

import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Mahir on 27.08.2017..
 */
public class MyLetter extends Pane {
    private ArrayList<MyLabel> labels = new ArrayList<>();
    private MyBoard myBoard;

    public MyLetter() {
        setPrefHeight(60);
        setPrefWidth(600 - 90);
        setStyle("-fx-background-color: #404040;");
    }

    public boolean addLetter(String letter, boolean isValid) {
        if (ifExists(letter)) {
            return false;
        }

        MyLabel label = new MyLabel();
        label.setText(letter);

        if (isValid) {
            label.setColor(ColorType.GREEN.color);
        } else {
            label.setColor(ColorType.RED.color);
        }
        getChildren().removeAll(labels);
        labels.add(label);
        Collections.sort(labels, new Comparator<MyLabel>() {
            @Override
            public int compare(MyLabel o1, MyLabel o2) {
                if (o1.getText().toCharArray()[0] > o2.getText().toCharArray()[0]) {
                    return 1;
                }
                if (o1.getText().toCharArray()[0] < o2.getText().toCharArray()[0]) {
                    return -1;
                }
                return 0;
            }
        });
        double maxlettersCount = Math.floor(510 / 45);
        double rowCount = Math.ceil(labels.size() / maxlettersCount);
        for (int j = 0; j < rowCount; j++){
            for (int i = 0; i < maxlettersCount; i++) {
                if (labels.size() <= (j * maxlettersCount) + i){
                    break;
                }
                labels.get((int) (j * maxlettersCount + i)).setLayoutX(20 + i * 45);
                if (j == 0){
                    labels.get((int) (j * maxlettersCount + i)).setLayoutY(17.5);
                } else {
                    labels.get((int) (j * maxlettersCount + i)).setLayoutY(j * 42.5 + 17.5);
                }
                getChildren().add(labels.get((int) (j * maxlettersCount + i)));
            }
        }
        if (rowCount == 1){
            setPrefWidth(labels.size() * 45 + 20);
            setPrefHeight(60);
        } else {
            setPrefWidth(510);
            setPrefHeight(rowCount*42.5 + 17.5);
        }
        return true;
    }

    private boolean ifExists(String word) {
        for (MyLabel myLabel : labels) {
            if (myLabel.getText().equals(word)) {
                return true;
            }
        }
        return false;
    }
}
