package hangman;

import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * Created by Mahir on 27.08.2017..
 */
public class MyBoard extends Pane implements HangmanWin{

    private ArrayList<MyLabel> myLabels;
    private int rowCount  = 1;
    public MyBoard() {
        setStyle("-fx-background-color: #404040;");
    }

    //TODO: Napraviti metod za splitanje rijeci na osnovu recenice koja se unese word.split(" ");


    public void createBoard(String word) {
        //TODO: Smaniti bord da ide do hangmana
        myLabels = new ArrayList<>();
        int numOfLetters = word.length();
        if (((numOfLetters * 45) + 20) > 430) {
            double r = (numOfLetters * 45.0) / 430.0;
            rowCount = (int) Math.ceil(r);
        } else {
            rowCount = 1;
        }
        int maxLettersInRow = (int) Math.ceil(430 / 45);
        for (int j = 0; j < rowCount; j++) {
            for (int i = 0; i < maxLettersInRow; i++) {
                if (word.length() <= (j * maxLettersInRow) + i) {
                    break;
                }
                MyLabel label = new MyLabel();
                if (String.valueOf(word.charAt((j * maxLettersInRow) + i)).equals(" ")){
                    label.setOpacity(0);
                    label.setText(" ");
                }
                label.setTag(String.valueOf(word.charAt((j * maxLettersInRow) + i)));
                label.setLayoutX(20 + i * 45);
                if (j == 0) {
                    label.setLayoutY(17.5);
                } else {
                    label.setLayoutY(17.5 + (25 + 17.5) * j);
                }

                myLabels.add(label);
                getChildren().add(label);
            }
            if (rowCount == 1){
                setPrefHeight(60);
                setPrefWidth(45*numOfLetters + 20);
            }else {
                setPrefWidth(45*maxLettersInRow + 20);
                setPrefHeight(42.5 * rowCount + 17.5);
            }
        }
        //TODO: napraviti sirinu da zavisi od broja slova, ako je broj slova manji od maxLettersInRow
//        for (int i = 0; i > maxLettersInRow; i++) {
//            if (word.length() == maxLettersInRow) {
//                setPrefWidth(maxLettersInRow);
//                setPrefHeight(60);
//
//            } else {
//                setPrefWidth(430);
//                setPrefHeight(rowCount * 43.5 + 17.5);
//
//            }
//
//        }
    }

    public boolean findLetter(String letter) {
        boolean result = false;
        for (MyLabel label : myLabels) {
            if (label.getTag().equals(letter)) {
                label.setText(label.getTag());
                result = true;
            }
        }
        if (isFilledUp()){
            for (MyLabel myLabel : myLabels) {
                myLabel.setColor(ColorType.GREEN.getColor());
            }
        }

        return result;
    }

    @Override
    public void onLose() {
       for (MyLabel myLabel : myLabels){
           if (myLabel.getText().equals("")){
               myLabel.setText(myLabel.getTag());
               myLabel.setColor(ColorType.RED.getColor());
           }
       }
    }
    private boolean isFilledUp(){
        for (MyLabel myLabel : myLabels){
            if (myLabel.getText().equals("")){
                return false;
            }
        }
        return true;
    }
}
