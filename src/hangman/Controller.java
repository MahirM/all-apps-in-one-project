package hangman;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {


    public PasswordField word;
    public TextField letter;
    public Button create;
    public Button ok;
    public MyBoard myboard;
    public MyLetter letters;
    public Hangman hangman;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        hangman.setMyBoard(myboard);
        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               myboard.createBoard(word.getText());

            }
        });

        ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                boolean isValid = myboard.findLetter(letter.getText());
                if (!letters.addLetter(letter.getText(), isValid) || !isValid){
                    hangman.showNext();
                }
                letter.clear();
            }

        });

    }
}
