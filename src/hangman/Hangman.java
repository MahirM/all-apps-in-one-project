package hangman;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;


/**
 * Created by Mahir on 27.08.2017..
 */
public class Hangman extends Pane {


    private Circle head;
    private Line body;
    private Line rightArm;
    private Line leftArm;
    private Line rightLeg;
    private Line leftLeg;
    private MyBoard myBoard;

    private int numberOfShown;


    public Hangman() {
        setPrefSize(80, 300);
        head = new Circle(20, Color.ORANGE);
        head.setCenterX(40);
        head.setCenterY(30);
        head.setOpacity(0.5);
        getChildren().add(head);

        body = new Line(39, 50, 41,200);
        body.setStyle("-fx-background-color: black");
        body.setOpacity(0.5);
        getChildren().add(body);

        rightLeg = new Line(40.5, 199, 60, 250);
        rightLeg.setOpacity(0.5);
        getChildren().add(rightLeg);

        leftLeg = new Line(40.5, 199, 20, 250);
        leftLeg.setOpacity(0.5);
        getChildren().add(leftLeg);

        leftArm = new Line(40.5, 100, 20, 150);
        leftArm.setOpacity(0.5);
        getChildren().add(leftArm);

        rightArm = new Line(40.5, 100, 60, 150);
        rightArm.setOpacity(0.5);
        getChildren().add(rightArm);
    }

    public Hangman(Node... children) {
        super(children);
    }
    public void showNext(){
        switch (numberOfShown) {
            case 0: {
                head.setOpacity(1);
                break;
            }
            case 1: {
                body.setOpacity(1);
                break;
            }
            case 2: {
                rightArm.setOpacity(1);
                break;
            }
            case 3: {
                leftArm.setOpacity(1);
                break;
            }
            case 4: {
                rightLeg.setOpacity(1);
                break;
            }
            case 5: {
                leftLeg.setOpacity(1);
                if (myBoard != null){
                    myBoard.onLose();
                }
                break;
            }
            default:
                break;
        }
            numberOfShown++;
        }

    public void setMyBoard(MyBoard myBoard) {
        this.myBoard = myBoard;
    }
}

