package equation;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    public Button generate;
    public TextField text1;
    public TextField text2;

    private int x;
    private int y;
    private int r;
    private int x2;
    private int y2;
    private int r2;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        generate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String j1=text1.getText();
                if (j1.substring(0, j1.indexOf("x")).equals("")){
                    x = 1;
                } else {
                    x = Integer.parseInt(j1.substring(0, j1.indexOf("x")));
                }
                System.out.println(x);

                if (j1.substring(j1.indexOf("x") + 1, j1.indexOf("y")).equals("+")){
                    y = 1;
                } else if (j1.substring(j1.indexOf("x") + 1, j1.indexOf("y")).equals("-")){
                    y = -1;
                } else {
                    y = Integer.parseInt(j1.substring(j1.indexOf("x") + 1, j1.indexOf("y")));
                }
                System.out.println(y);
                r = Integer.parseInt(j1.substring(j1.indexOf("=") + 1, j1.length()));
                System.out.println(r);

                String j2=text2.getText();
                if (j2.substring(0, j2.indexOf("x")).equals("")){
                    x2 = 1;
                } else {
                    x2 = Integer.parseInt(j2.substring(0, j2.indexOf("x")));
                }
                System.out.println(x2);

                if (j2.substring(j2.indexOf("x") + 1, j2.indexOf("y")).equals("+")){
                    y2 = 1;
                } else if (j1.substring(j1.indexOf("x") + 1, j1.indexOf("y")).equals("-")){
                    y2 = -1;
                } else {
                    y2 = Integer.parseInt(j2.substring(j2.indexOf("x") + 1, j2.indexOf("y")));
                }
                System.out.println(y2);
                r2 = Integer.parseInt(j2.substring(j2.indexOf("=") + 1, j2.length()));
                System.out.println(r2);



            }
        });

    }
}
