package sample.memory;


import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;

/**
 * Created by Mahir on 30.08.2017..
 */
public class MyButton extends Button {

     private String tag;
     private String color = ColorType.GREEN.getColor();
     private Boolean shouldStop = false;


     public MyButton(){
         setStyle("-fx-background-color: " + color );
         setTextFill(Color.BLACK);
         setPrefSize(70, 70);
         setMaxWidth(Double.MAX_VALUE);
         setAlignment(Pos.CENTER);


     }


    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        setStyle("-fx-background-color: " + color);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyButton button = (MyButton) o;

        return tag.equals(button.tag);
    }

    public void showText(String text){
        setText(tag);
        shouldStop=true;
    }

    public Boolean getShouldStop() {
        return shouldStop;
    }

    public void setShouldStop(Boolean shouldStop) {
        this.shouldStop = shouldStop;
    }
}
