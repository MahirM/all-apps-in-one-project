package sample.memory;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

import java.util.*;
import java.util.Timer;


/**
 * Created by Mahir on 30.08.2017..
 */
public class Board extends Pane {


    private ArrayList<MyButton> myButtons;
    private MyButton previousButton = null;
    private Label timeLabel;
    private MyButton button;
    private Board board;


    public Board() {
        setStyle("-fx-background-color: #404040;");
    }

    public void createBoard(int numberofrows, int numberofcolumns, String patterns) {
        myButtons = new ArrayList<>();
        manipulateWithTimeLabel();
        for (int row = 1; row <= numberofrows; row++) {
            for (int col = 1; col <= numberofcolumns; col++) {
                MyButton button = new MyButton();
                button.setId(String.valueOf(myButtons.size()));
                myButtons.add(button);
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        showText(button);
                        toggle(button);
                    }
                });
                getChildren().add(button);
                button.setLayoutX((col * 80) - 80);
                if (row == 0) {
                    button.setLayoutY(17.5);
                } else {
                    button.setLayoutY((80 * row) - 80);
                }
            }
            setTags(generateList(patterns));
        }
    }

    public boolean findLetter(String letter) {
        boolean result = false;
        for (MyButton myButton : myButtons) {
            if (myButton.getTag().equals(letter)) {
                myButton.setText(myButton.getTag());
                result = true;
            }
        }
        return result;
    }

    private String [] getFromPatterns(String pattern){
        return pattern.split(",");
    }
    private ArrayList<String> generateListOfStrings(String pattern){
        String [] strings = getFromPatterns(pattern);
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < strings.length; i++){
            arrayList.add(strings[i]);
        }
        for (int i = 0; i < strings.length; i++){
            arrayList.add(strings[i]);
        }

        ArrayList<Integer> numList = generateRandomNumberList(2* strings.length);
        ArrayList<String> resultList = new ArrayList<>();

        for (int j = 0; j < numList.size(); j++){
            resultList.add(arrayList.get(numList.get(j)));
        }
        System.out.println(resultList.toString());
        return resultList;
    }

    private ArrayList<Integer> generateRandomNumberList(int count){
        ArrayList<Integer> list = new ArrayList<>();
        while (list.size() < count){
            Random random = new Random();
            int num = random.nextInt(count);
            if (!list.contains(num)){
                list.add(num);
            }
        }
        System.out.println(list.toString());
        return list;
    }

    private ArrayList<String> generateList(String patterns) {
        String[] strings = getFromPatterns(patterns);
        ArrayList<String> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, strings);
        Collections.addAll(arrayList, strings);
        Collections.shuffle(arrayList);
        return arrayList;
    }
    public void setTags(ArrayList<String> list){
        for (MyButton myButton : myButtons){
            int id = Integer.parseInt(myButton.getId());
            myButton.setTag(list.get(id));
        }
    }
    public void toggle(MyButton button){
        if (button.equals(previousButton)){
            previousButton.setShouldStop(true);
            button.setShouldStop(true);
            previousButton.setText(previousButton.getTag());
            button.setText(button.getTag());
            previousButton = null;
        } else {
            previousButton = button;
        }
    }

    private void showText(MyButton button){
        button.setText(button.getTag());
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (!button.getShouldStop()){
                            button.setText("");
                        }
                    }
                });
            }
        };
        new Timer().schedule(timerTask, 1000);
    }
    @Override
    public String toString() {
        return "Board{" +
                "myButtons=" + myButtons +
                '}';
    }
    public boolean isAllFieldsFilledUp() {
        for (MyButton myButton : myButtons) {
            if (myButton.getText().equals("")) {
                return false;
            }
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Memory Game");
                alert.setHeaderText("You won in: " + timeLabel.getText()+" seconds");
                alert.setContentText("Do you want another game?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK){
                    for (MyButton myButton : myButtons){
                        getChildren().removeAll(myButton);
                        timeLabel.setText("0");
                    }
                }
            }
        });
        return true;
    }

    public Label getTimeLabel() {
        return timeLabel;
    }

    public void setTimeLabel(Label timeLabel) {
        this.timeLabel = timeLabel;
    }

    private void manipulateWithTimeLabel(){
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (!isAllFieldsFilledUp()){
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            timeLabel.setText(String.valueOf(Integer.parseInt(timeLabel.getText()) + 1));
                        }
                    });
                    manipulateWithTimeLabel();
                }
            }
        };
        new Timer().schedule(timerTask, 1000);

    }



    }


