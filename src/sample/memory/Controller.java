package sample.memory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public TextField numofrows;
    public TextField numofcolumns;
    public TextField pattern;
    public Button create;
    public Board myboard;
    public Label time;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        myboard.setTimeLabel(time);
        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int numberofrows = Integer.parseInt(numofrows.getText());
                int numberofcolumns = Integer.parseInt(numofcolumns.getText());
                String patterns = pattern.getText();
                myboard.createBoard(numberofrows, numberofcolumns, patterns);
            }
        });
        
    }
}




