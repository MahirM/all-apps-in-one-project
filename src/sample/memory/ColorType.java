package sample.memory;

/**
 * Created by Mahir on 30.08.2017..
 */
public enum ColorType {

    RED("#FF0000"),
    GREEN("#00FF00"),
    GREY("#8B8B8B");

    String color;
    ColorType(String s)  {color = s;
    }
    public String getColor(){return color;}
    public void setColor(String color) {this.color = color;}
}

