package main;
import hangman.Hangman;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.layout.Background;
import javafx.stage.Stage;
import javax.swing.*;
import java.net.URL;
import java.util.*;

public class Controller implements Initializable  {

    public Button one;
    public Button two;
    public Button three;
    public Pane pane;
    public Button four;
    public BackgroundImage myBI;
    public Button eq;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        one.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            new sample.memory.Main().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        two.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            new xo.Main().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        three.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            new hangman.Main().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        four.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            new calculator.Main().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
        eq.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                  Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            new equation.Main().start(new Stage());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        one.setStyle("-fx-background-radius: 8px");
        two.setStyle("-fx-background-radius: 8px");
        three.setStyle("-fx-background-radius: 8px");
        four.setStyle("-fx-background-radius: 8px");
        eq.setStyle("-fx-background-radius: 8px");




    }
}






